# Jnr Software Engineer Technical Exercise

The solution was delivered in GENBASIC and the solution is available for testing in POPOWIP along with the automated test script.
The solution consists of:
- A wrapper program file(so that a user can provide inputs at the command line)
- A subroutine file that contains the core functionality that evaluates the user's inputs, reads the xml file and generates the csv files
  accordingly. This subroutine is also what is tested by the automated test script.
- Lastly an automated test script that can be run from the command line and will evaluate Pass or Fail based on the test cases.

How to use the solution:
In a command line terminal that is connected to POPOWIP you can run 'METERDATANOTI.PROGRAM.VK -FP <File.Folder> -F <File.name.xml>'.
If the program is run on its own without any input arguements, some help text will be displayed that will advise how to use the
program. The output files generated will also be placed in the same folder as the input file.

How to use the automated test script:
In a command line terminal that is connected to POPOWIP you can run 'TSTSUB.METERDATANOTI.CONVERT.VK SUB' (this will run all test cases).
If you want to run the test cases individually, you can use 'TSTSUB.METERDATANOTI.CONVERT.VK SUB <Test case ID>' (i.e where the test case id is MDN001, MDN002 or MDN003)
The test cases I have included are:
[MDN001] File does not exist in the specified folder
[MDN002] File contains single transaction with two expected output files
[MDN003] File contains single transaction with three expected output files

I faced some difficulty with processing files that contained multiple transactions or a single transaction containing data that would spawn more than three csv files.
Given more time I would have resolved this issue as one of the GENBASIC functions (XMLDATA) was throwing an error that I did not have the right access to look into.
Also with more time, I would have added more automated tests that performed both positive and negative tests. I had included in the solution some basic user input validation
which I did not include automated tests for (only covered the main test in MDN001).