SUBROUTINE METERDATANOTI.CONVERT.VK(IN COMMAND.LINE, OUT ERR.MSG)
    EQU SOURCE.STANDARDS TO 0
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This subroutine contains the main logic for the METERDATANOTI.PROGRAM.VK
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   $INCLUDE EMS-DEFS TOTAL.COM
   $INCLUDE MOD-DEFS XML.COM
   *
   VAR GO.AHEAD = TRUE
   *
   CALL CHECK.COMMAND.LINE(COMMAND.LINE, GO.AHEAD, VAR FILE.LOCATION, VAR FILE.NAME, ERR.MSG)
   IF NOT(GO.AHEAD) THEN 
      RETURN
   END
   * Validate the File path and File name by attempting to retrieve the input file
   VAR INPUT.SEQ.FILE
   FOPENSEQ FILE.LOCATION, FILE.NAME READONLY WAITING 0 TO INPUT.SEQ.FILE THEN 
      *
      VAR XML(XML$MATRIX$SIZE)
      VAR DATABLK
      * Attempt to parse the xml to ensure there are no errors
      TRY
         SEEK 0 FROM BEGIN ON INPUT.SEQ.FILE
         READBLK DATABLK FROM INPUT.SEQ.FILE THEN
            CALL XMLBEGINPARSE(MAT XML, '', DATABLK)
            CALL XMLPARSE(MAT XML, FILE.LOCATION)
            CALL XMLENDPARSE(MAT XML)
         END
      CATCH VAR ERR.DESC
         IF ERR.DESC # '' THEN
            ERR.MSG = ERR.DESC
            RETURN
         END
      END
      * Close as we no longer need the input xml file
      CLOSESEQ INPUT.SEQ.FILE
   END ELSE
      ERR.MSG = 'Could not open the directory ':FILE.LOCATION:': File name "':FILE.NAME:'"'
      RETURN
   END
   *
END SUBROUTINE
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This subroutine will parse the xml file
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE XMLPARSE(MAT XML, IN FILE.LOCATION)
   CALL XMLENTERELEMENTNEXT(MAT XML, VAR ASETAG)
   CALL XMLENTERELEMENTNEXT(MAT XML, VAR HEADER)
   * Future work could look to add more validations or use the header information
   CALL XMLLEAVEELEMENT(MAT XML, HEADER)
   CALL XMLENTERELEMENTOPTIONAL(MAT XML, 'Transactions', VAR EXISTS)
   IF EXISTS THEN
      VAR REPEAT.NUM
      LOOP
         CALL XMLENTERELEMENTREPEAT(MAT XML, 'Transaction', REPEAT.NUM)
      WHILE REPEAT.NUM DO
         CALL PARSE.TRANSACTION(MAT XML, VAR INTERVAL.DATA)
         * Now that we have our data of interest its time to loop through and create the CSV files
         CALL CSV.FILE.CREATE(INTERVAL.DATA, FILE.LOCATION)
         * Now that we have our data of interest its time to loop through and create the CSV files
         CALL XMLLEAVEELEMENT(MAT XML, 'Transaction')
      REPEAT
      CALL XMLLEAVEELEMENT(MAT XML, 'Transactions')
   END
   CALL XMLLEAVEELEMENT(MAT XML, ASETAG)
END SUBROUTINE
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This subroutine will create and update the output csv files.
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE CSV.FILE.CREATE(IN INTERVAL.DATA, IN FILE.LOCATION)
   VAR OUTPUT.SEQ.FILE
   VAR CSV.OPEN = FALSE
   VAR HEADER.LINE = INTERVAL.DATA<1>
   VAR TRAILER.LINE = INTERVAL.DATA<DCOUNT(INTERVAL.DATA, AM)>
   FOR VAR I = 1 TO DCOUNT(INTERVAL.DATA, AM)
      * Read data line
      VAR DATA.LINE = INTERVAL.DATA<I>
      VAR DATA.TAG = FIELD(DATA.LINE, ',', 1)
      * Determine next steps
      BEGIN SELECT DATA.TAG
         WHEN 200
            * Close an existing file (if one exists)
            IF CSV.OPEN THEN
               WRITESEQ TRAILER.LINE TO OUTPUT.SEQ.FILE ELSE ABORT 'Error writing to file'
               CLOSESEQ OUTPUT.SEQ.FILE
               CSV.OPEN = FALSE
            END
            *
            * Create new CSV file
            VAR OUTPUT.FILE.NAME = FIELD(DATA.LINE, ',', 2):'.csv'
            FOPENSEQ FILE.LOCATION, OUTPUT.FILE.NAME READWRITE WAITING 0 TO OUTPUT.SEQ.FILE THEN
               WRITESEQ HEADER.LINE TO OUTPUT.SEQ.FILE ELSE ABORT 'Error writing to file'
               WRITESEQ DATA.LINE TO OUTPUT.SEQ.FILE ELSE ABORT 'Error writing to file'
               CSV.OPEN = TRUE
            END
         WHEN 300
            * Update existing file
            WRITESEQ DATA.LINE TO OUTPUT.SEQ.FILE ELSE ABORT 'Error writing to file'
         OTHERWISE
            * Do nothing
      END SELECT
      * 
   NEXT I
   * Close the final file in the transaction
   WRITESEQ TRAILER.LINE TO OUTPUT.SEQ.FILE ELSE ABORT 'Error writing to file'
   CLOSESEQ OUTPUT.SEQ.FILE
END SUBROUTINE
*----
* Process Transaction
*----
SUBROUTINE PARSE.TRANSACTION(IN OUT MAT XML, IN OUT INTERVAL.DATA)
   CALL XMLENTERELEMENTNEXT(MAT XML, VAR TAGNAME)
   IF TAGNAME = 'MeterDataNotification' THEN
      CALL XMLENTERELEMENTOPTIONAL(MAT XML, 'CSVIntervalData', VAR EXISTS)
      IF EXISTS THEN
         * XMLDATA trims trailing/leading whitespace
         CALL XMLDATA(MAT XML, INTERVAL.DATA)
         * Convert CHAR(9) to tabs
         INTERVAL.DATA = CHANGE(INTERVAL.DATA, CHAR(9), '   ')
         * Convert "," to attribute marks
         INTERVAL.DATA = CHANGE(INTERVAL.DATA, CHAR(10), AM)
         CALL XMLLEAVEELEMENT(MAT XML, 'CSVIntervalData')
      END
      CALL XMLENTERELEMENTNEXT(MAT XML, VAR PARTICIPANT)
      * Future work could look to add more validations or use the participant role information
      CALL XMLLEAVEELEMENT(MAT XML, PARTICIPANT)
      CALL XMLLEAVEELEMENT(MAT XML, TAGNAME)
   END
END SUBROUTINE
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This subroutine will validate the user's input to ensure a xml file exists in the specified file path 
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE CHECK.COMMAND.LINE(IN COMMAND.LINE, IN OUT GO.AHEAD, OUT FILE.LOCATION, OUT FILE.NAME, OUT ERR.MSG)
   ERR.MSG = ''
   *
   IF COMMAND.LINE = 'METERDATANOTI.PROGRAM.VK' THEN COMMAND.LINE := AM:'-H'
   *
   * Some instructions on how to use this program.
   CALL SEARCHINPUT(COMMAND.LINE, '-H', VAR RESULT)
   IF RESULT = 'Help Text' THEN
      CRT 'METERDATANOTI.PROGRAM.VK Usage:'
      CRT '-H                           -> Help text'
      CRT '-FP <File Location Path>     -> Specify the folder in EMS-TFR (file path to this folder: \\rome\ems\popo\popowip\EMS-TFR) where the xml file is located. This is also where the output csv files will be created.'
      CRT '-F <Filename>                -> The input xml file to be processed'
      CRT 'See code documentation for further information.'
      *
      GO.AHEAD = FALSE
      RETURN
      *
   END
   * Search the user input for the file path marker
   CALL SEARCHINPUT(COMMAND.LINE, '-FP', FILE.LOCATION)
   IF FILE.LOCATION = '' THEN
      ERR.MSG = 'File Location is missing. Please specify which EMS-TFR (file path to this folder: \\rome\ems\popo\popowip\EMS-TFR) folder to retrieve the xml file from. This is also where the output csv files will be created.'
      GO.AHEAD = FALSE
      RETURN
   END
   *
   * Search the user input for the file name
   CALL SEARCHINPUT(COMMAND.LINE, '-F', FILE.NAME)
   IF FILE.NAME = '' THEN
      ERR.MSG = 'Input file is missing. Please specify the file name of the xml file to be processed.'
      GO.AHEAD = FALSE
      RETURN
   END
   * Basic check to ensure file atleast contains the .xml suffix
   IF FIELD(UPCASE(FILE.NAME), '.', 2) # 'XML' THEN
      ERR.MSG = 'The specified file does not contain the "xml" suffix, please ensure the file being loaded is an xml file with the correct suffix.'
      GO.AHEAD = FALSE
      RETURN
   END
   *
END SUBROUTINE
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
* This subroutine looks through the user input for the specified marker and retrieves the value next to the marker
*-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
SUBROUTINE SEARCHINPUT(IN COMMAND.LINE, IN MARKER, OUT RESULT)
   RESULT = ''
   LOCATE MARKER IN COMMAND.LINE SETTING VAR POS THEN
      RESULT = COMMAND.LINE<POS+1>
      IF MARKER = '-H' THEN
         RESULT = 'Help Text'
      END
   END
END SUBROUTINE
